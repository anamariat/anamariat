## Hello👋! 

#### My name is Ana-Maria and I'm a 25 years old Junior Java Developer currently living in Iasi, Romania. Welcome to my GitLab page!

I am an organized and goal-directed person, and I always try to get an in-depth understanding of the new topics I encounter. I think everyone can learn anything if they put in enough effort to fully understand the subject they are interested in.

I am very passionate about programming and I strive to better myself as a developer. I seek new opportunities to expand my knowledge in Java. Besides, I am currently learning JavaScript, HTML and CSS, because I am aspiring to become a Full-Stack Developer. 

**Fun facts about me**
- I spend my free time reading books 📚, crafting ✂️ and creating designs in Adobe Illustrator and Photoshop 🖥️ (mostly templates for wedding invitations)  
- I have a degree in Medicine 💉

📫 If you want to get in touch, please contact me on [LinkedIn](https://www.linkedin.com/in/ana-maria-%C8%9Bibuleac-597b431ba/) or by e-mail at anamaria.tibuleac@yahoo.com
